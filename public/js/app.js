var botViewer = angular.module('botViewer', ['ngMaterial', 'ngCookies','nvd3']);

botViewer.controller('BodyController', ['$scope', '$cookies', function($scope, $cookies, BotServerFactory){
    $scope.newConfig = function(data) {
        var add = true;
        for (var i=0;i<$scope.configs.length;i++) {
            if ($scope.configs[i].url==data.url) {
                add = false;
                break;
            }
        }
        if (add) {
            $scope.configs.push(angular.copy(data));
            $cookies.putObject('configs', $scope.configs);
            $cookies.put('pass', $scope.pass);
        }
    }
    
    $scope.savePass = function() {
        $cookies.put('pass', $scope.pass);
    }
    
    $scope.clear = function() {
        $scope.configs = [];
        $cookies.putObject('configs', $scope.configs);
    }
    $scope.configs = $cookies.getObject('configs');
    $scope.pass = $cookies.get('pass');
    if (!$scope.configs) {
        $scope.clear();
    }
}])
.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.transformRequest.push(function(requestData) {
        return requestData;
    });
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
    $httpProvider.interceptors.push('authInterceptor');
}])
.factory('authInterceptor', ['$rootScope', '$q', '$cookies', function ($rootScope, $q, $cookies) {
    return {
        request: function (config) {
            config.params = config.params || {};
            var token = $cookies.get('pass');
            config.params.p = token;
            return config;
        },
        responseError: function (rejection) {
            return $q.reject(rejection) || $q.when(rejection);
        }
    };
}])
.controller('BotServerCardController', ['$scope', 'BotServerFactory', '$timeout', '$http', function($scope, BotServerFactory, $timeout, $http) {
    $scope.botServer = new BotServerFactory($scope.config);

        $scope.betGraphOptions = {
            chart: {
                type: 'multiBarHorizontalChart',
                height: 400,
                
                x: function(d){return d.label;},
                y: function(d){return d.value;},
                showControls: true,
                showValues: true,
                duration: 500,
                xAxis: {
                    showMaxMin: false
                },
                yAxis: {
                    axisLabel: 'Values',
                    tickFormat: function(d){
                        return d3.format(',0f')(d);
                    }
                }
            }
        };
        $scope.betGraphData = [
            {
                    "key": "Repeats",
                    "color": "#d62728",
                    "values": []
            },
            {
                    "key": "Bets (en miles)",
                    "color": "#1f77b4",
                    "values": []
            },        
            {
                    "key": "Chences de que te coja",
                    "color": "#FFFFF",
                    "values": []
            }
        ];

    function fillGrapStreak1(scope){
                scope.betGraphData[0].values=[];
                scope.betGraphData[1].values=[];
                scope.betGraphData[2].values=[];
                for(var i = 4; i<=14; i++){
                    
                    var totalBetCount = scope.botServer.data.info.bets;
                    var prob = 7.06426 * Math.pow(10,-7) * Math.pow(i,13.1789);
                    var lastSeenOnBet = scope.botServer.data.info.lastStreak[i].bets > 0 ?  scope.botServer.data.info.lastStreak[i].bets : prob;
                    var perc=0;
                    

                    perc =   (100 *  (totalBetCount -prob ) / lastSeenOnBet  ) ;
                    
                    scope.betGraphData[0].values.push(  {"label":"Streak " + i, "value":scope.botServer.data.info.lastStreak[i].repeat} );
                    scope.betGraphData[1].values.push(  {"label":"Streak " + i, "value":scope.botServer.data.info.lastStreak[i].bets /1000 }  );
                    scope.betGraphData[2].values.push(  {"label":"Streak " + i, "value": perc }  );
                }
    };
    function tick () {
        $scope.botServer.updateData(function() {
            if ($scope) {
                fillGrapStreak1($scope);
               

                $timeout(tick, 100);
                
            }
        })
    }
    tick();
}])
.factory('BotServerFactory', ['$http', function($http) {
    var BotServer = function(config) {
        this.url = config.url;
        this.token = config.token;
        this.targetMoney = config.targetMoney;
        this.safeStreak = config.safeStreak;
        this.maxCalls = config.maxCalls;
        this.waitForSafeStreak = config.waitForSafeStreak;
        this.safePercentage = config.safePercentage;
        this.data = {info:null};
        this.serverStatus = {info: null};
        this.exchangeRate = {usd: null}
    }
    
    function makeUpdateFunction(objectToFill, callback) {
        return function (res) {
            objectToFill.info = res.data;
            if (callback) callback();
        };
    }
    
    function makePrototypeFunction(method, objectToFill, path, add) {
        return function(callback) {
            var obj = add?this: undefined;
            method(this.url+path, obj).then(makeUpdateFunction(this[objectToFill], callback));
        }
    }
    
    BotServer.prototype.updateData = makePrototypeFunction($http.get, "data", "/data");
    BotServer.prototype.status = makePrototypeFunction($http.get, "serverStatus", '/server_status');
    BotServer.prototype.initialize = makePrototypeFunction($http.post, "data", '/initialize', true);
    BotServer.prototype.start = makePrototypeFunction($http.post, "serverStatus", '/start');
    BotServer.prototype.stop = makePrototypeFunction($http.post, "serverStatus", '/stop');
    BotServer.prototype.updateExchangeRate = makePrototypeFunction($http.get, "exchangeRate", '/exchange_rate');
    
    return BotServer;
}]);